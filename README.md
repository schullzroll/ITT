# Semestrální projekt
Autor: Roland Schulz (xschul06)
Literatura: [Zotero](https://www.zotero.org/groups/5237059/xschul06_bakalarka/library)

# ToDo
- implementuj max možný počet řádků tabulce
- pokus se přeložit model na SMT

|index|address|value|stored|accessed|
|-----|-------|-----|------|--------|
|    1|     _a|    5|   yes|     yes|
|    2|     _a|    6|   yes|      no|
|    3|     _a|    3|   yes|     yes|
|    4|     _a|    9|   yes|      no|
|    5|     _a|    9|   yes|      no|
|    6|     _a|    9|   yes|      no|
|    7|     _a|    9|   yes|     yes|

