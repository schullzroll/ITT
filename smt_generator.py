import sys
import pysmt
import re
from pysmt.typing import INT, REAL, BOOL
from pysmt.shortcuts import Int, Symbol, And, Equals, Implies, Not, Or, Plus, Times, Minus, get_model, LT, GT, GE, LE, is_unsat, is_sat
from mem_model import MemoryModel

class Expression():
    def __init__(self, e: str, symbols: dict):
        self.exp = e
        self.symbols = symbols
    
    def to_smt(self, mem_model: MemoryModel):
        tokens = self.exp.strip().split()
        first = tokens[0]
        # either 1 or 3
        #print(f"{tokens} -> {len(tokens)}")
        if len(tokens) == 1:
            # if is int
            if first.isdigit():
                return Int(int(first))
            # TODO: add support for real
            if first.isidentifier():
                return self.symbols[mem_model.get_freshestOr(first, first).address]
        
        if len(tokens) == 3:
            op = tokens[1]
            third = tokens[2]
            known_ops = {
                "+": Plus,
                "-": Minus,
                "*": Times,
            }
            return known_ops[op](
                Expression(first, self.symbols).to_smt(mem_model),
                Expression(third, self.symbols).to_smt(mem_model)
            )
        

known_typing = {
    "int": INT,
    "real": REAL,
    "bool": BOOL
}
def declare(line) -> tuple[str, Symbol]:
    """ Take a line of input and generate a pysmt symbol.
    "int a" -> Symbol("a", INT)
    "real b" -> Symbol("b", REAL)
    "bool c" -> Symbol("c", BOOL)
    """
    match = re.match(r"(\w+) (\w+)", line.strip())
    if match:
        typing, name = match.groups()
        return (name, Symbol(name, known_typing[typing])) if typing in known_typing else (None, None)

    return (None, None)


def constraint(line, symbols: dict, mem_model: MemoryModel):
    tokens = line.strip().split()
    if not tokens:
        return None
    symbol = tokens[0]
    relational_operators = {
        "=": (lambda v: Equals(v[0], v[1])),
        "==": (lambda v: Equals(v[0], v[1])),
        "!=": (lambda v: Not(Equals(v[0], v[1]))),
        "<": (lambda v: LT(v[0], v[1])),
        "<=": (lambda v: LE(v[0], v[1])),
        ">": (lambda v: GT(v[0], v[1])),
        ">=": (lambda v: GE(v[0], v[1])),
    }

    # relational with expression
    exp = " ".join(tokens[2:])
    op = tokens[1]
    smt_exp = Expression(exp, symbols).to_smt(mem_model)
    
    # assignment changes next values in memory model
    # need to update the symbol to point to the new value
    if op == "=":
        new_name, expression = mem_model.update_value(symbol, smt_exp)
        old_symbol = symbols[symbol]
        symbols[new_name] = Symbol(new_name, old_symbol.symbol_type())
        return Equals(symbols[new_name], expression)

    return relational_operators[op]([symbols[mem_model.get_freshestOr(symbol, symbol).address], smt_exp])

if __name__ == "__main__":
    symbols = dict()
    constraints = list()
    mem = MemoryModel()

    # parse declarations
    while True:
        line = sys.stdin.readline().strip()
        if line.startswith("#"):
            continue
        name, declaration = declare(line)
        if name and declaration:
            symbols[name] = declaration
        else:
            break
    
    # parse constraints
    while True:
        #print(constraints)
        #print("====")
        line = sys.stdin.readline().strip()
        if line.startswith("#"):
            continue
        if not line:
            break
        constraints.append(constraint(line, symbols, mem))
    
    # generate model
    formula = And(constraints) # to CNF (conjunctive normal form)
    print(formula)
    if is_unsat(formula):
        print("not SAT")
        exit(1)
    model = get_model(formula)
    for _, symbol in symbols.items():
        print(f"{symbol.symbol_name()} = {model.get_value(symbol)}")
    exit(0)
    