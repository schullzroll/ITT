from copy import deepcopy
from os import path
from networkx import DiGraph
from typing import Any, final
from sys import setrecursionlimit, argv

from pysmt.shortcuts import is_sat, is_unsat, get_model, And

from smt_generator import constraint, declare, known_typing
from mem_model import MemoryModel

class Location(str):
    pass

Loc = Location

class Type(str):
    pass

T = Type

class Variable(str):
    pass

Var = Variable

class Op(str):
    pass

def backtrack(G: DiGraph,
              ffrom: Location,
              end: Location,
              mmodel: MemoryModel,
              constraints: list,
              level: int,
              bound: int,
              symbols: dict,
              path_accumulator: list[Location]
              ) -> tuple[bool, dict[Variable, Any], list, list[Location]]:
    this_path = path_accumulator + [ffrom]
    #print("processing " + "->".join(str(p) for p in this_path) + f" at level {level}")
    if level > bound:
        print("->".join(str(p) for p in this_path) + " exhausted")
        return False, {}, constraints, this_path

    formula = And(constraints)
    model = get_model(formula)
    if model is None:
        #print("->".join(str(p) for p in this_path) + " unsat " + str(constraints))
        #return False, {}, constraints, this_path
        pass
    
    if ffrom == end and model: # found a path
        return True, {var.symbol_name(): model.get_value(var) for var in symbols.values()}, constraints, this_path
    
    visit = list(G.out_edges(ffrom))
    for ffrom, to in visit:
        data = G.get_edge_data(ffrom, to)
        op = data["op"]
        if any(op.startswith(t) for t in known_typing.keys()):
            name, declaration = declare(op)
            symbols[name] = declaration
            visit.extend(G.out_edges(to))
            continue
        mmcopy = deepcopy(mmodel)
        new_constraints = constraints + [constraint(op, symbols, mmcopy)]
        found_path, evaluation, retcons, path = backtrack(G, to, end, mmcopy, new_constraints, mmcopy.store_count(), bound, symbols, path_accumulator + [ffrom])
        if found_path:
            return found_path, evaluation, retcons, path

    return False, {}, constraints, this_path # exhausted all paths

class CFA():
    def __init__(self,
                 Q: set[Location],
                 q_in: Location,
                 q_out: Location,
                 X: set[Variable],
                 edges: set[tuple[Location, Op, Location]]
                 ) -> None:
        self.Q = Q
        self.q_in = q_in
        self.q_out = q_out
        self.X = X
        self.edges = edges
        self.graph = self.build_graph()
    
    def build_graph(self) -> DiGraph:
        G = DiGraph()
        for location in self.Q:
            G.add_node(location)
        for (ffrom, constraint, to) in self.edges:
            G.add_edge(ffrom, to, op=constraint)
        return G
    
    def terminates(self, bound=10) -> tuple[bool, dict[Variable, Any], list, list[Location]]:
        mmodel = MemoryModel()
        return backtrack(self.graph, self.q_in, self.q_out, mmodel, list(), 0, bound, dict(), [])

if __name__ == "__main__":
    setrecursionlimit(10000)
    # bound
    N = int(argv[1]) if len(argv) > 1 else 100

    # Create a CFA from sutre example
    # q1 to q12
    q = {i: Loc(f"q{i}") for i in range(-3, 13)}
    declaration_locations = len([_ for index, _ in q.items() if index < 0])
    N = max(N, declaration_locations + 1)
    input_vars = {k: Var(k) for k in ["y"]}
    vars = {k: Var(k) for k in ["x"]}
    start = q[(min(q.keys()))]
    err = q[12]
    edges = {
            (q[-3], Op("int x"), q[-2]),
            (q[-2], Op("int y"), q[-1]),
            (q[-1], Op("y < 214748347"), q[1]),
            (q[1], Op("x = 1"), q[2]),
            (q[2], Op("y > 10"), q[6]),
            #(q[2], Op("y <= 10"), q[3]),
            #(q[3], Op("y = 10"), q[11]),
            (q[6], Op("x >= y"), q[11]),
            (q[6], Op("x < y"), q[7]),
            (q[7], Op("x = 2 * x"), q[8]),
            (q[8], Op("y = y - 1"), q[6]),
            (q[11], Op("x = y + 1"), q[12]),
    }

    cfa = CFA(
        Q=set(q.values()),
        q_in=start,
        q_out=err,
        X=set(vars.values()) | set(input_vars.values()),
        edges=edges
    )

    sat, evaluation, constraints, path = cfa.terminates(N)
    print()
    print(f"N = {N}")
    print("Path found " if sat else "No path found\n", end="")
    print("->".join(str(p) for p in path))
    print("Path length: " + str(len(path)))
    print("Constraints: " + str(constraints))
    print("SSA Evaluation: " + str(evaluation))
    print("Evaluation of input variables:")
    print(f"y = {evaluation.get('y')}")
    eval = sorted([(k, evaluation.get(k)) for k in evaluation], key=lambda x: x[0])
    print(eval)