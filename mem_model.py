
class AddressValuePair():
    def __init__(self, address, value) -> None:
        self.address = address
        self.value = value

class MemoryModel():
    def __init__(self) -> None:
        self.timeline = dict()
    
    def get_freshest(self, address):
        return self.timeline[address][-1]

    def store_count(self) -> int:
        # number of times an update was made
        return sum(len(self.timeline[address]) for address in self.timeline)
    
    def get_freshestOr(self, address, default):
        if address not in self.timeline:
            return AddressValuePair(address, default)
        return self.get_freshest(address)
    
    def get_all(self, address):
        return self.timeline[address]
    
    def update_value(self, address, value):
        entries = len(self.timeline.get(address, []))
        unique_address = f"{address}_{entries}"
        av_pair = AddressValuePair(unique_address, value)
        if address not in self.timeline:
            self.timeline[address] = list()
        self.timeline[address].append(av_pair)
        return av_pair.address, av_pair.value
