# Purpose: unroll a while loop int a series of constraints
#
# basically just copy the constraints by the nnumber of iterations,
# interleave the predicate constraints and
# end it with the negation of the predicate.
# Should handle nested while loops

import sys
import re
from typing import List, Tuple, Callable, Optional, Any, Dict, Union
from smt_generator import Symbol, Expression, INT, REAL, BOOL, Equals, Not, LT, LE, GT, GE


def negate_predicate(predicate: str) -> str:
    symbol, relation, rest = predicate.split(maxsplit=2)
    negated_relation = {"<": ">=",
                        "<=": ">",
                        ">": "<=",
                        ">=": "<",
                        "==": "!=",
                        "=": "!=",
                        "!=": "=="
                        }[relation]
    return " ".join([symbol, negated_relation, rest])

class While():
    def __init__(self, predicate: str, body: List):
        self.predicate = predicate
        self.body = body

    def __str__(self):
        return f"while ({self.predicate}) {{\n" + "\n".join(self.body) + "\n}"

    def __repr__(self):
        return str(self)
    
    def unroll(self, iterations: int) -> List[str]:
        """ Unroll this while loop into a list of lines repeated by the number of iterations
        """
        constraints = list()
        for i in range(iterations):
            constraints.append(self.predicate)
            for line in self.body:
                constraints.append(line)

        constraints.append(negate_predicate(self.predicate))
        return constraints

def create_while_body(f = sys.stdin) -> List[str]:
    body = list()
    while True:
        line = f.readline()
        strip = line.strip()
        if strip.startswith("while"):
            body.append("\n".join(parse_while(strip, f)))
        if strip.startswith(("end", "}")):
            break
        body.append(strip)
    return body



def parse_while(line: str, f = sys.stdin) -> List[str]:
    predicate = line.strip()[7:].strip()[0:-1]
    body = create_while_body(f)
    while_loop = While(predicate, body)
    iterations = N
    return while_loop.unroll(iterations)

def parse_line(line: str, f = sys.stdin):
    if line.startswith("while"):
        while_constraints = parse_while(line, f)
        return("\n".join(while_constraints))
    else:
        return(line)

N = 10

if __name__ == "__main__":
    if len(sys.argv) > 1:
        N = int(sys.argv[1])

    for line in sys.stdin:
        print(parse_line(line.strip()))
