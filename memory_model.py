#/bin/python3

import sys

def yesno(pred: bool):
    return "yes" if pred else "no"

class MemoryModel():
    def __init__(self):
        self.rows = list() # stack

    def register(self, op: str, address: str, value: str):
        return {
                "LOAD": self.load,
                "STORE": self.store
                }[op.upper()](address, value)

    def load(self, address: str, value: str):
        address_rows = [row for row in self.rows if row.address == address]
        if not address_rows:
            raise Exception(f"Read before write for {address} with value {value}")
        last_address_store = address_rows[-1]
        last_address_store.load_from(value)

    def store(self, address: str, value: str):
        new_store_row = MemoryModel.MemoryRow(len(self.rows)+1, address, value)
        self.rows.append(new_store_row)

    def get_rows(self, pred = lambda r: True):
        return [row for row in self.rows if pred(row)]

    class MemoryRow():
        columns = {
            "index": lambda mr: str(mr.index),
            "address": lambda mr: mr.address or '',
            "value": lambda mr: mr.value or '',
            "stored": lambda mr: yesno(mr.in_use),
            "accessed": lambda mr: yesno(mr.accessed)
        }

        def __init__(self, index: int, address: str = None, value: str = None):
            self.index = index
            self.address = address
            self.value = value
            self.in_use = True if bool(address) and bool(value) else False
            self.accessed = False

        def load_from(self, value: str):
            self.accessed = True

        @classmethod
        def handlers(cls):
            return cls.columns.values()

        @classmethod
        def handler(cls, column: str):
            return cls.columns.get(column)

        @classmethod
        def column_names(cls) -> dict[str,callable]:
            return cls.columns.keys()
        

class MemoryModelMarkdownView():
    @classmethod
    def draw(cls, mm: MemoryModel):
        mrc = mm.MemoryRow
        header = "|".join(list(mrc.column_names()) + [""]) + "\n"
        header += "=" * len(header) + "\n"
        formatted_rows = list()
        for row in mm.rows:
            row_columns = [mrc.handler(caption)(row).rjust(len(caption), ' ') for caption in mrc.column_names()]
            formatted_rows.append('|'.join(row_columns + [""]) + "\n")
        return header + ''.join(formatted_rows)

    @classmethod
    def show(cls, mm: MemoryModel):
        print(cls.draw(mm))

if __name__ == "__main__":
    model = MemoryModel()
    view = MemoryModelMarkdownView()

    # processes lines in form of
    # {LOAD,STORE} addr val
    for line in (l for l in sys.stdin if l.strip() and not l.startswith("#")):
        op, address, value = map(str.strip, line.split(" "))
        model.register(op, address, value)

    view.show(model)